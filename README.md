# Roller Coaster Challenge Tools

This is a collection of scripts to generate, validate and solve puzzles of Roller Coaster Challenge.
They use the the [Roller Coaster Challenge Schema](https://gitlab.com/roller-coaster-challenge/roller-coaster-challenge-schema) to store the puzzles.

## Generate a challenge

_TODO_

## Validate a track

_TODO_

## Solve a challenge

_TODO_

## License

[Roller Coaster Challenge](https://www.thinkfun.com/products/roller-coaster-challenge/) is a Trademark of ThinkFun and has been invented by Oli Morris. This 100% fan project is not affiliated with ThinkFun nor Oli Morris. No assets with their copyright have been used.

Copyright (C) 2019  The Roller Coaster Challenge Tools authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
