export const arePositionsEqualGenerator = (pos1) => (pos2) => (
  pos1.x === pos2.x && pos1.y === pos2.y && pos1.height === pos2.height
);

export function findPieceAt({trackPieces}, position) {
  const isCorrect = arePositionsEqualGenerator(position);
  return (trackPieces || []).find(isCorrect);
}

export function validateTrackCompleteness(track) {
  throw Error('NOT IMPLEMENTED');
}
