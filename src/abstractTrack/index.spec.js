import {
  determineHeight,
  generateTrack,
} from './index.js';

describe('determineHeight', () => {
  test('default height is 1', () => {
    const track = [
      { length: 1, drop: 0, },
    ];
    expect(determineHeight(track)).toBe(1);
  });
  test('adds up a simple drop of 2', () => {
    const track = [
      { length: 1, drop: 2, },
    ];
    expect(determineHeight(track)).toBe(3);
  });
  test('adds up two simple drops of 2 and 3', () => {
    const track = [
      { length: 1, drop: 2, },
      'r',
      { length: 1, drop: 3, },
    ];
    expect(determineHeight(track)).toBe(6);
  });
});

describe('generateTrack', () => {
  test('matches snapshot for track 01', () => {
    const track = [
      { length: 3, drop: 1, },
      'l',
      { length: 2, drop: 1, },
    ];
    expect(generateTrack(track)).toMatchSnapshot();
  });
  test('matches snapshot for track 02', () => {
    const track = [
      { length: 4, drop: 1, },
      'l',
      { length: 1, drop: 1, },
    ];
    expect(generateTrack(track)).toMatchSnapshot();
  });
  test('matches snapshot for track 03', () => {
    const track = [
      { length: 2, drop: 2, },
      'r',
      { length: 2, drop: 1, },
    ];
    expect(generateTrack(track)).toMatchSnapshot();
  });
  test('matches snapshot for track 04', () => {
    const track = [
      { length: 3, drop: 1, },
      'l',
      { length: 3, drop: 2, },
      'l',
      { length: 3, drop: 1, },
    ];
    expect(generateTrack(track)).toMatchSnapshot();
  });
  test('matches snapshot for track 05', () => {
    const track = [
      { length: 4, drop: 1, },
      'r',
      { length: 2, drop: 1, },
      'r',
      { length: 2, drop: 2, },
      'r',
      { length: 3, drop: 2, },
    ];
    expect(generateTrack(track)).toMatchSnapshot();
  });
  test('matches snapshot for track 06', () => {
    const track = [
      { length: 2, drop: 3, },
      'l',
      { length: 2, drop: 2, },
      'l',
      { length: 2, drop: 1, },
      'r',
      { length: 1, drop: 1, },
      'r',
      { length: 2, drop: 2, },
    ];
    expect(generateTrack(track)).toMatchSnapshot();
  });
});