import {
  inventory,
} from './fixtures/inventory.js';
import {
  getAccumulatedPostHeight,
  validatePostPositions,
} from './validator/validatePosts.js';
import {
  generateValidDefaultTrack,
} from './generate.js';

describe('generateValidDefaultTrack', () => {
  for (let i = 0; i < 1000; i++) {
    describe(`test: ${i}`, () => {
      const track = generateValidDefaultTrack(inventory);
      test('has the correct dimensions', () => {
        expect(track.xLength).toBe(5);
        expect(track.yLength).toBe(5);
      });
      test('has valid post positions', () => {
        expect(validatePostPositions(track)).toBe(true);
      });
      test('uses no more than 36 posts', () => {
        expect(getAccumulatedPostHeight(track)).toBeLessThanOrEqual(36);
      });
    });
  }
});
