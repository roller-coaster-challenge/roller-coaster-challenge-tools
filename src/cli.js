import fs from 'node:fs';

import {
  inventory,
} from './fixtures/inventory.js';
import {
  generateValidDefaultTrack,
} from './generate.js';

const track = generateValidDefaultTrack(inventory);
fs.writeFileSync('track.json', JSON.stringify(track, null, 2), 'utf8');
