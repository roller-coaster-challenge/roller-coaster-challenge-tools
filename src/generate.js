import {
  determineDimensions,
  determineHeight,
  generateTrack,
} from './abstractTrack/index.js';
import {
  raisePosts,
} from './manipulations/raisePosts.js';
import {
  translateTrack,
} from './manipulations/translate.js';

function getRandomInt(min, max) {
  return Math.floor(Math.random()*(max-min))+min;
}

function pickRandom(array) {
  return array[Math.floor(Math.random()*array.length)];
}

function removeRandom(array) {
  return array.splice(Math.floor(Math.random()*array.length), 1)[0];
}

function generateAbstractTrack({inventory, length}) {
  length = length || 3;
  const abstractTrack = [];
  for (var i = length; i > 0; i--) {
    abstractTrack.push(removeRandom(inventory.straightTracks));
    if (i > 1) abstractTrack.push(removeRandom(inventory.curvedTracks));
  }
  return abstractTrack;
}

function isValidDefaultTrack(abstractTrack) {
  const { minX, minY, maxX, maxY } = determineDimensions(abstractTrack);
  const width = maxX - minX;
  const height = maxY - minY;
  const postHeight = determineHeight(abstractTrack);
  return (width <= 5 && height <= 5 && postHeight >= 2);
}

export function generateValidDefaultTrack(inventory) {
  let abstractTrack;
  const length = getRandomInt(2, 5);

  do {
    abstractTrack = generateAbstractTrack({
      inventory: {
        curvedTracks: [...inventory.curvedTracks],
        straightTracks: [...inventory.straightTracks],
      },
      length,
    });
  } while(!isValidDefaultTrack(abstractTrack));

  const startDirection = pickRandom('NESW');
  let track = generateTrack(abstractTrack, startDirection);
  const { minX, minY, maxX, maxY } = determineDimensions(abstractTrack, startDirection);
  const width = maxX - minX;
  const height = maxY - minY;
  track = translateTrack(track, -minX, -minY);
  track = translateTrack(track, getRandomInt(0, 5 - width), getRandomInt(0, 5 - height));
  track.xLength = 5;
  track.yLength = 5;
  track = raisePosts(track);
  return track;
}
