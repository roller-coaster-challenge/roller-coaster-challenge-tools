import {
  getCurves,
  getEnd,
  getStart,
} from '../queries/filters.js';
import {
  rotateDirection,
} from './rotate.js';

function getPostPositionOfCurvedTrack({x, y, direction, turn}) {
  if (!['right', 'left'].includes(turn)) throw new Error(`Unknown turn: ${turn}`);
  if (!'NESW'.includes(direction)) throw new Error(`Unknown direction: ${direction}`);
  if (turn === 'right') {
    if (direction === 'N') return { x: x, y };
    if (direction === 'E') return { x: x - 1 , y };
    if (direction === 'S') return { x: x - 1, y: y - 1 };
    if (direction === 'W') return { x, y: y - 1 };
  } else if (turn === 'left') {
    if (direction === 'N') return { x: x - 1, y };
    if (direction === 'E') return { x: x - 1, y: y - 1 };
    if (direction === 'S') return { x, y: y - 1 };
    if (direction === 'W') return { x, y };
  }
}

export function raisePost(track, x, y, height) {
  track.posts = track.posts || [];
  const post = track.posts.find((post) => (
    post.x === x && post.y === y
  ));
  if (post && post.height < height) {
    post.height = height;
  }
  if (!post) {
    track.posts.push({ x, y, height });
  }
  return track;
}

function raisePostsForStart(track, toTheLeft = false) {
  const start = getStart(track.trackPieces);
  if (start.x === 0 && start.direction === 'S') {
    toTheLeft = true;
  }
  if (start.y === 0 && start.direction === 'W') {
    toTheLeft = true;
  }
  if (start.x === track.xLength && start.direction === 'N') {
    toTheLeft = true;
  }
  if (start.y === track.yLength && start.direction === 'E') {
    toTheLeft = true;
  }
  const {x, y} = getPostPositionOfCurvedTrack({
    x: start.x,
    y: start.y,
    direction: rotateDirection(start.direction, toTheLeft ? 1 : 2),
    turn: 'left',
  });
  return raisePost(track, x, y, start.height);
}

function raisePostsForEnd(track, toTheLeft = false) {
  const end = getEnd(track.trackPieces);
  if (end.x === 0 && end.direction === 'S') {
    toTheLeft = true;
  }
  if (end.y === 0 && end.direction === 'W') {
    toTheLeft = true;
  }
  if (end.x === track.xLength && end.direction === 'N') {
    toTheLeft = true;
  }
  if (end.y === track.yLength && end.direction === 'E') {
    toTheLeft = true;
  }
  const {x, y} = getPostPositionOfCurvedTrack({
    x: end.x,
    y: end.y,
    direction: end.direction,
    turn: toTheLeft ? 'left' : 'right',
  });
  return raisePost(track, x, y, end.height);
}

export function raisePosts(track, leftOfStart, leftOfEnd) {
  let resultTrack = track;
  getCurves(track.trackPieces).forEach((curvedTrack) => {
    const {x, y} = getPostPositionOfCurvedTrack(curvedTrack);
    resultTrack = raisePost(resultTrack, x, y, curvedTrack.height);
  });
  if (getStart(track.trackPieces)) {
    resultTrack = raisePostsForStart(resultTrack, leftOfStart);
  }
  if (getEnd(track.trackPieces)) {
    resultTrack = raisePostsForEnd(resultTrack, leftOfEnd);
  }
  return resultTrack;
}
