import {
  rotateDirection,
} from './rotate.js';

describe('rotateDirection', () => {
  test('1 rotation from N is E', () => {
    expect(rotateDirection('N', 1)).toBe('E');
  });
  test('2 rotations from N is S', () => {
    expect(rotateDirection('N', 2)).toBe('S');
  });
  test('3 rotations from N is W', () => {
    expect(rotateDirection('N', 3)).toBe('W');
  });
  test('4 rotations from N is N', () => {
    expect(rotateDirection('N', 4)).toBe('N');
  });
  test('17 rotations from N is E', () => {
    expect(rotateDirection('N', 17)).toBe('E');
  });
  test('-1 rotations from N is W', () => {
    expect(rotateDirection('N', -1)).toBe('W');
  });
  test('1 rotation from E is S', () => {
    expect(rotateDirection('E', 1)).toBe('S');
  });
  test('1 rotation from S is W', () => {
    expect(rotateDirection('S', 1)).toBe('W');
  });
  test('1 rotation from W is N', () => {
    expect(rotateDirection('W', 1)).toBe('N');
  });
});
