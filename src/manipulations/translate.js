const translateObjects = (objects, xShift, yShift) => {
  if (!objects) return undefined;
  return objects.map((object) => ({
    ...object,
    x: object.x + xShift,
    y: object.y + yShift,
  }));
};

export function translateTrack(track, xShift, yShift) {
  return {
    ...track,
    posts: translateObjects(track.posts, xShift, yShift),
    trackPieces: translateObjects(track.trackPieces, xShift, yShift),
    tunnels: translateObjects(track.tunnels, xShift, yShift),
  };
}
