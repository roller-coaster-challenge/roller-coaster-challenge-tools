/**
 * Returns how a direction changes after 90° rotations.
 */
export function rotateDirection(direction, rotations) {
  const directions = 'NESW';
  const index = directions.indexOf(direction);
  const newIndex = (((index + rotations) % 4 ) + 4 ) % 4;
  return directions[newIndex];
}
