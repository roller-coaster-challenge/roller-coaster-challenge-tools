export const createCurve = (params) => ({
  type: 'curve',
  x: 0,
  y: 0,
  height: 1,
  direction: 'N',
  turn: 'right',
  ...params,
});

export const createEnd = (params) => ({
  type: 'end',
  x: 0,
  y: 0,
  height: 1,
  direction: 'N',
  ...params,
});

export const createStart = (params) => ({
  type: 'start',
  x: 0,
  y: 0,
  height: 1,
  direction: 'N',
  ...params,
});

export const createStraight = (params) => ({
  type: 'straight',
  x: 0,
  y: 0,
  height: 1,
  direction: 'N',
  length: 1,
  drop: 0,
  ...params,
});

export const createTrack = (params) => ({
  xLength: 5,
  yLength: 5,
  posts: [],
  trackPieces: [],
  tunnels: [],
  ...params,
});
