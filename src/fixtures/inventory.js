export const inventory = {
  curvedTracks: 'rrrrrrrrllllllll'.split(''),
  straightTracks: [
    { length: 1, drop: 0, }, // magenta
    { length: 1, drop: 0, },
    { length: 1, drop: 1, }, // green
    { length: 1, drop: 1, },
    { length: 1, drop: 1, },
    { length: 2, drop: 1, },
    { length: 2, drop: 1, },
    { length: 2, drop: 1, },
    { length: 3, drop: 1, },
    { length: 3, drop: 1, },
    { length: 4, drop: 1, },
    { length: 4, drop: 1, },
    { length: 2, drop: 2, }, // blue
    { length: 2, drop: 2, },
    { length: 3, drop: 2, },
    { length: 3, drop: 2, },
    { length: 4, drop: 2, },
    { length: 4, drop: 2, },
    { length: 2, drop: 3, }, // orange
    { length: 2, drop: 3, },
  ],
};
